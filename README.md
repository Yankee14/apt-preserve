# What is `apt-preserve`?

`apt-preserve` is a minimalistic, suckless tool for IT professionals and network administrators to serve \*.deb packages to workstations on the network. `apt-preserve` strives to reduce internet bandwidth consumption by downloading workstation package(s) *only one time* on the server, instead of one time *per unique workstation*.

# How does `apt-preserve` work?

Workstations running the `apt install` family of commands will transparently request the local server to provide the relevant packages and dependencies that the workstation requires, instead of the Debian package mirrors.

* If the server already has all of the requested \*.deb packages in the backup, no download takes place, and the required \*.deb packages are transferred to the workstation for installation.
* If the server is missing one or more of the requested \*.deb packages in the backup, it will download those packages that are missing from the official mirror, add them to the backup, and transfer them to the workstation for installation.

Note that `apt-preserve` **does not require any users on any workstations to learn any new commands or methods to install packages**. From their perspective, `apt` will function exactly as it would on any other machine.

`apt-preserve`:

* saves internet bandwidth by only downloading packages from the internet once, even if 100 Linux workstations need the same package
* indirectly logs and backs up all packages (and their dependenices) which were downloaded and installed on user workstations
* avoids the complexity and overhead of running a full local Debian mirror (which can require several terabytes) just to serve a comparatively small number of packages to workstations
* does not require regular workstation users to learn any new commands or methods

# Background

At the end of 2020, I was in a strange situation where I had to install and setup (Debian) Linux on 3 workstations and 1 server--using only the hotspot on my phone for internet to download packages. With a very limited data connection, I needed a simple way to ensure that apt did not download the packages repeatedly on each workstation and the server. Thus, `apt-preserve` was bo(u)rn(e).

# Requirements

1. The server preserving the *.deb packages AND the workstations must all target the same release. For example, if the workstations are targeting `stable`, the server must also target `stable`.

    (note: it should be possible for the server and workstation release targets to differ with some small modifications to the download scripts)

# Installation

1. Install the apt-preserve-server-vX.deb on the local server that will host the \*.deb files.
2. Install the apt-preserve-client-vX.deb on each workstation that will request \*.deb files from the server.

### Server Configuration

* The server will enable and use the default user `backup` for downloading, backing up, and serving the requested packages.
* Requested packages will be backed up in the directory `/srv/apt-preserve`

### Client Notes

The client configuration file is located at `/etc/apt/preserve.d/apt-preserve.conf`. You must set:

* the IP address or hostname of the server hosting the packages
* the (SSH) server user managing the downloading and backing up of packages (default `backup`)
* the SSH server port (default `22`)
* the directory (default `/srv/apt-preserve`)

# Caveat emptor

1. The author does not really understand the security implications of the use of the `backup` user, so merge requests or issue suggestions are especially welcome on this topic.
