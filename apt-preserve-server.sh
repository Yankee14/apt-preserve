#!/bin/bash

# CONSTANTS
declare -rg APT_PRESERVE_SUDOERS="/etc/sudoers.d/10_apt-preserve"

# GLOBALS

declare -g BACKUP_USER="apt-preserve"
declare -g BACKUP_DIRECTORY="/srv/apt-preserve"
declare -g PREVENT_APT_AUTOCLEAN="y"

function enable_backup_user()
{
    # prompt for backup user, default 'backup'
    read -e -p "Enter user preserving *.deb files [${BACKUP_USER}]: " BACKUP_USER
    BACKUP_USER="${BACKUP_USER:-apt-preserve}"
    printf -- "User '${BACKUP_USER}' chosen to preserve *.deb files\n\n"

    # if $BACKUP_USER does not exist
    id --user "${BACKUP_USER}" > /dev/null 2>&1
    declare -ri backup_user_exists=$?
    if [ $backup_user_exists -ne 0 ]; then
        create_new_user
    else
        printf -- "User '${BACKUP_USER}' already exists. Please double check account security measures such as shell, chroot, etc.\n"
    fi
    printf -- "\n"

    # prompt for new password
    declare -- change_password="y"
    read -e -p "Create new password for '${BACKUP_USER}'? [Y/n]: " change_password
    change_password="${change_password:-y}"
    if [ "${change_password}" = "Y" ] || [ "${change_password}" = "y" ]; then
        #stty -echo
        #declare -- new_password=""
        #printf -- "New password: "
        #read new_password
        #declare -- new_password_confirm=""
        #printf -- "Retype new password: "
        #read new_password_confirm
        #stty echo
        sudo passwd "${BACKUP_USER}"
    fi
    #declare -r password_status="$(sudo passwd --status "${BACKUP_USER}" | cut --delimiter=' ' --field=2)"
    #if [ "${password_status}" = "L" ] || [ "${password_status}" = "NP" ]; then
    #    printf -- "Account locked\n"
    #fi
    printf -- "\n"
}

function create_new_user()
{
    printf -- "User '${BACKUP_USER} does not exist. Creating limited user\n"

    # create user account disabled, restricted shell
    adduser --disabled-login --shell /bin/rbash --gecos "" "${BACKUP_USER}" 

    # remove all files, i.e. .bashrc
    rm --verbose --recursive --force "/home/${BACKUP_USER}/".* 2>/dev/null

    # create limited set of allowed executables in custom $PATH
    sudo mkdir --verbose --parents "/home/${BACKUP_USER}/bin"
    sudo chown 0:0 "/home/${BACKUP_USER}/bin"
    sudo chmod 755 "/home/${BACKUP_USER}/bin"
    sudo ln --verbose --symbolic "$(which sudo)" "/home/${BACKUP_USER}/bin/sudo"
    sudo ln --verbose --symbolic "$(which rsync)" "/home/${BACKUP_USER}/bin/rsync"
    sudo ln --verbose --symbolic "$(which apt)" "/home/${BACKUP_USER}/bin/apt"

    # create .profile with limited custom $PATH
    printf -- "PATH=\"\$HOME/bin\"\n" | sudo tee "/home/${BACKUP_USER}/.profile"
    sudo chown --verbose 0:0 "/home/${BACKUP_USER}/.profile"
}

function create_sudoers_rules()
{
    # create sudoers.d entry for allowing 'sudo apt install --download-only *'
    if [ ! -d "/etc/sudoers.d" ]; then
        sudo mkdir --verbose /etc/sudoers.d
        sudo chown 0:0 /etc/sudoers.d
        sudo chmod 755 /etc/sudoers.d
    fi
    printf -- \
"${BACKUP_USER} ALL=(ALL) NOPASSWD: $(which apt) install --download-only --reinstall *
${BACKUP_USER} ALL=(ALL) NOPASSWD: $(which apt) update\n" | sudo tee "${APT_PRESERVE_SUDOERS}"
    sudo chown 0:0 "${APT_PRESERVE_SUDOERS}"
    sudo chmod 440 "${APT_PRESERVE_SUDOERS}"
}

function create_backup_directory()
{
    # prompt for backup directory, default '/srv/apt-preserve'
    read -e -p "Enter directory that should preserve *.deb files [${BACKUP_DIRECTORY}]: " BACKUP_DIRECTORY
    BACKUP_DIRECTORY="${BACKUP_DIRECTORY:-/srv/apt-preserve}"
    printf -- "Directory '${BACKUP_DIRECTORY}' chosen to preserve *.deb files\n\n"

    sudo mkdir --verbose --parents "${BACKUP_DIRECTORY}"
    sudo chown --verbose "${BACKUP_USER}:${BACKUP_USER}" "${BACKUP_DIRECTORY}"
    printf -- "\n"
}

function prevent_apt_autoclean()
{
    read -e -p "Prevent apt from autocleaning *.deb files at '/var/cache/apt/archives'? [Y/n]: " PREVENT_APT_AUTOCLEAN
    PREVENT_APT_AUTOCLEAN="${PREVENT_APT_AUTOCLEAN:-y}"

    if [ "${PREVENT_APT_AUTOCLEAN}" = "Y" ] || [ "${PREVENT_APT_AUTOCLEAN}" = "y" ]; then
        printf -- "Configuring apt to stop autocleaning *.deb files\n"
        printf -- "Binary::apt::APT::Keep-Downloaded-Packages \"1\";\n" | sudo tee /etc/apt/apt.conf.d/10apt-keep-downloads
        printf -- "Please remember to periodically clean the apt cache\n"
    fi
        printf -- "\n"
}

function perform_initial_backup()
{
    declare -- initial_backup="y"
    read -e -p "Perform initial *.deb file backup? [Y/n]: " initial_backup
    initial_backup="${initial_backup:-y}"

    if [ "${initial_backup}" = "Y" ] || [ "${initial_backup}" = "y" ]; then
        sudo rsync --archive --human-readable --progress --info=progress2 --itemize-changes /var/cache/apt/archives/*.deb "${BACKUP_DIRECTORY}/"
        sudo chown --verbose "${BACKUP_USER}:${BACKUP_USER}" "${BACKUP_DIRECTORY}"/*.deb
    fi
    printf -- "\n"
}

function install()
{
    enable_backup_user
    create_sudoers_rules
    create_backup_directory
    prevent_apt_autoclean
    perform_initial_backup
}

function uninstall()
{
    sudo rm --verbose --force "${APT_PRESERVE_SUDOERS}"
}

function main()
{
    if [ "${1}" = "install" ] || [ $# -eq 0 ]; then
        printf -- "Installing apt-preserve server\n\n"
        install
        printf -- "apt-preserve server setup complete\n"
    elif [ "${1}" = "uninstall" ]; then
        printf -- "Uninstalling apt-preserve server\n\n"
        uninstall
        printf -- "apt-preserve server uninstall complete\n"
    fi
    printf -- "\n"
}

main "${@}"

