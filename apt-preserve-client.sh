#!/bin/bash

# CONSTANTS
declare -rg APT_PRESERVE_CONF_DIR="/etc/apt/preserve.d"
declare -rg APT_PRESERVE_CONF_FILE_SOURCE="./apt-preserve.conf"
declare -rg APT_PRESERVE_CONF_FILE_DESTINATION="${APT_PRESERVE_CONF_DIR}/apt-preserve.conf"

declare -rg APT_PRESERVE_BINARY_SOURCE="./apt"
declare -rg APT_PRESERVE_BINARY_DESTINATION="/usr/local/bin/apt"

# GLOBALS

function create_default_conf()
{
    printf -- "Creating default configuration file\n"
    sudo mkdir --verbose "${APT_PRESERVE_CONF_DIR}"
    sudo chown --verbose 0:0 "${APT_PRESERVE_CONF_DIR}"
    sudo chmod 755 "${APT_PRESERVE_CONF_DIR}"

    sudo cp --verbose "${APT_PRESERVE_CONF_FILE_SOURCE}" "${APT_PRESERVE_CONF_FILE_DESTINATION}"
    sudo chown --verbose 0:0 "${APT_PRESERVE_CONF_FILE_DESTINATION}"
    sudo chmod 644 "${APT_PRESERVE_CONF_FILE_DESTINATION}"

    printf -- "\n"
    printf -- "Default configuration file created at ${APT_PRESERVE_CONF_FILE_DESTINATION}\n\n"
}

function create_replacement_apt()
{
    printf -- "Creating replacement apt\n\n"
    sudo cp "${APT_PRESERVE_BINARY_SOURCE}" "${APT_PRESERVE_BINARY_DESTINATION}"
    sudo chown --verbose 0:0 "${APT_PRESERVE_BINARY_DESTINATION}"
    sudo chmod 755 "${APT_PRESERVE_BINARY_DESTINATION}"
    printf -- "Replacement apt created at ${APT_PRESERVE_BINARY_DESTINATION}\n\n"
}

function install()
{
    create_default_conf
    create_replacement_apt
}

function uninstall()
{
    sudo rm --verbose --recursive --force "${APT_PRESERVE_CONF_DIR}" "${APT_PRESERVE_BINARY_DESTINATION}"
}

function main()
{
    if [ "${1}" = "install" ] || [ $# -eq 0 ]; then
        printf -- "Installing apt-preserve client\n\n"
        install
        printf -- "apt-preserve client setup complete. Please review conf file before using 'apt', and make sure your path contains the entry '/usr/local/bin' before '/usr/bin'!\n"
    elif [ "${1}" = "uninstall" ]; then
        printf -- "Uninstalling apt-preserve client\n\n"
        uninstall
        printf -- "apt-preserve client uninstall complete\n"
    fi
}

main "${@}"

